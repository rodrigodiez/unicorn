import unicornhat
import time
import Image
import ImageDraw
from random import randint

def set_image(image):
  imwidth, imheight = image.size
  if imwidth != 8 or imheight != 8:
    raise ValueError('Image must be an 8x8 pixels in size.')

  # Convert image to RGB and grab all the pixels.
  pix = image.convert('RGB').load()

  # Loop through each pixel and write the display buffer pixel.
  for x in [0, 1, 2, 3, 4, 5, 6, 7]:
    for y in [0, 1, 2, 3, 4, 5, 6, 7]:
      color = pix[(x, y)]

      unicornhat.set_pixel(x, y, *color)

def smileyHappy(color,bgcolor):
  image = Image.new('RGB', (8, 8))
  draw = ImageDraw.Draw(image)

  draw.rectangle(((0,0),(7,7)),outline=color,fill=bgcolor)

  draw.point((2,2),fill=color)
  draw.point((5,2),fill=color)

  draw.point((2,4),fill=color)
  draw.point((3,5),fill=color)
  draw.point((4,5),fill=color)
  draw.point((5,4),fill=color)

  return image

def smileySad(color,bgcolor):
  image = Image.new('RGB', (8, 8))
  draw = ImageDraw.Draw(image)

  draw.rectangle(((0,0),(7,7)),outline=color,fill=bgcolor)
  draw.point((2,2),fill=color)
  draw.point((5,2),fill=color)

  draw.line((2,4,5,4),fill=color)

  return image

def zero():
  return [(0,0), (1,0), (2,0), (3,0), (0,1), (3,1), (0,2), (3,2), (0,3), (3,3), (0,4), (1,4), (2,4), (3,4)]

def one():
  return [(3,0), (2,1), (3,1), (3,2), (3,3), (3,4)]

def two():
  return [(0,0), (1,0), (2,0), (3,0), (3,1), (0,2), (1,2), (2,2), (3,2), (0,3), (0,4), (1,4), (2,4), (3,4)]

def three():
  return [(0,0), (1,0), (2,0), (3,0), (3,1), (1,2), (2,2), (3,2), (3,3), (0,4), (1,4), (2,4), (3,4)]

def four():
  return [(0,0), (3,0), (0,1), (3,1), (0,2), (1,2), (2,2), (3,2), (3,3), (3,4)]

def five():
  return [(0,0), (1,0), (2,0), (3,0), (0,1), (0,2), (1,2), (2,2), (3,2), (3,3), (0,4), (1,4), (2,4), (3,4)]

def six():
  return [(0,0), (1,0), (2,0), (3,0), (0,1), (0,2), (1,2), (2,2), (3,2), (0,3), (3,3), (0,4), (1,4), (2,4), (3,4)]

def seven():
  return [(0,0), (1,0), (2,0), (3,0), (3,1), (3,2), (3,3), (3,4)]

def eight():
  return [(0,0), (1,0), (2,0), (3,0), (0,1), (3,1), (0,2), (1,2), (2,2), (3,2), (0,3), (3,3), (0,4), (1,4), (2,4), (3,4)]

def nine():
  return [(0,0), (1,0), (2,0), (3,0), (0,1), (3,1), (0,2), (1,2), (2,2), (3,2), (3,3), (3,4)]


def tick():
  return [(7,0), (6,1), (7,1),(5,2), (6,2), (7,2), (0,3), (1,3), (4,3), (5,3), (6,3), (7,3), (0,4), (1,4), (2,4), (3,4), (4,4), (5,4), (6,4), (0,5), (1,5), (2,5), (3,5), (4,5), (5,5),(1,6), (2,6), (3,6), (4,6), (2,7), (3,7)]

def cross():
  return[(0,0), (1,0), (6,0), (7,0), (0,1), (1,1), (2,1), (5,1), (6,1), (7,1), (1,2), (2,2), (3,2), (4,2), (5,2), (6,2), (2,3), (3,3), (4,3), (5,3), (2,4), (3,4), (4,4), (5,4), (1,5), (2,5), (3,5), (4,5), (5,5), (6,5), (0,6), (1,6), (2,6), (5,6), (6,6), (7,6), (0,7), (1,7), (6,7), (7,7)]

def warning():
  return[(3,0), (4,0), (3,1), (4,1), (3,2), (4,2), (3,3), (4,3), (3,4), (4,4), (3,6), (4,6), (3,7), (4,7)]

def smileyAngry(color,bgcolor):
  image = Image.new('RGB', (8, 8))
  draw = ImageDraw.Draw(image)

  draw.rectangle(((0,0),(7,7)),outline=color,fill=bgcolor)

  draw.point((2,2),fill=color)
  draw.point((5,2),fill=color)

  draw.rectangle((2,4,5,5),fill=color)

  return image

unicornhat.brightness(0.8)
unicornhat.rotation(180)

smileys = []

sad = smileySad((255,255,0), (0,0,0))
happy = smileyHappy((0,255,0), (0,0,0))
angry = smileyAngry((255,0,0), (0,0,0))

smileySets = [happy, sad, angry]


while(True):
  for smileySet in smileySets:
      set_image(smileySet)

      unicornhat.show()
      time.sleep(0.3)
