import unicornhat
import sys
import Image
import ImageDraw
import time

def set_image(image):
	imwidth, imheight = image.size
	if imwidth != 8 or imheight != 8:
		raise ValueError('Image must be an 8x8 pixels in size.')
	
	# Convert image to RGB and grab all the pixels.
	pix = image.convert('RGB').load()
	
	# Loop through each pixel and write the display buffer pixel.
	for x in [0, 1, 2, 3, 4, 5, 6, 7]:
		for y in [0, 1, 2, 3, 4, 5, 6, 7]:
			color = pix[(x, y)]
			
			unicornhat.set_pixel(x, y, *color)

path = sys.argv[1]
image = Image.open(path)
resized = image.resize((8,8))

unicornhat.brightness(0.3)
unicornhat.rotation(180)

set_image(resized)
unicornhat.show()

time.sleep(100)
