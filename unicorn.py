import unicornhat
import time
import Image
import ImageDraw
from random import randint

def set_image(image):
	imwidth, imheight = image.size
	if imwidth != 8 or imheight != 8:
		raise ValueError('Image must be an 8x8 pixels in size.')
	
	# Convert image to RGB and grab all the pixels.
	pix = image.convert('RGB').load()
	
	# Loop through each pixel and write the display buffer pixel.
	for x in [0, 1, 2, 3, 4, 5, 6, 7]:
		for y in [0, 1, 2, 3, 4, 5, 6, 7]:
			color = pix[(x, y)]
			
			unicornhat.set_pixel(x, y, *color)

def smileyHappy(color,bgcolor):
	image = Image.new('RGB', (8, 8))
	draw = ImageDraw.Draw(image)

	draw.rectangle(((0,0),(7,7)),outline=color,fill=bgcolor)

	draw.point((2,2),fill=color)
	draw.point((5,2),fill=color)
	
	draw.point((2,4),fill=color)
	draw.point((3,5),fill=color)
	draw.point((4,5),fill=color)
	draw.point((5,4),fill=color)

	return image

def smileySad(color,bgcolor):
	image = Image.new('RGB', (8, 8))
	draw = ImageDraw.Draw(image)

	draw.rectangle(((0,0),(7,7)),outline=color,fill=bgcolor)
	draw.point((2,2),fill=color)
	draw.point((5,2),fill=color)
	
	draw.line((2,4,5,4),fill=color)

	return image

def zero():
	return [(0,0), (1,0), (2,0), (3,0), (0,1), (3,1), (0,2), (3,2), (0,3), (3,3), (0,4), (1,4), (2,4), (3,4)]

def one():
	return [(3,0), (2,1), (3,1), (3,2), (3,3), (3,4)]

def two():
	return [(0,0), (1,0), (2,0), (3,0), (3,1), (0,2), (1,2), (2,2), (3,2), (0,3), (0,4), (1,4), (2,4), (3,4)]

def three():
	return [(0,0), (1,0), (2,0), (3,0), (3,1), (1,2), (2,2), (3,2), (3,3), (0,4), (1,4), (2,4), (3,4)]

def four():
	return [(0,0), (3,0), (0,1), (3,1), (0,2), (1,2), (2,2), (3,2), (3,3), (3,4)]

def five():
	return [(0,0), (1,0), (2,0), (3,0), (0,1), (0,2), (1,2), (2,2), (3,2), (3,3), (0,4), (1,4), (2,4), (3,4)]

def six():
	return [(0,0), (1,0), (2,0), (3,0), (0,1), (0,2), (1,2), (2,2), (3,2), (0,3), (3,3), (0,4), (1,4), (2,4), (3,4)]

def seven():
	return [(0,0), (1,0), (2,0), (3,0), (3,1), (3,2), (3,3), (3,4)]

def eight():
	return [(0,0), (1,0), (2,0), (3,0), (0,1), (3,1), (0,2), (1,2), (2,2), (3,2), (0,3), (3,3), (0,4), (1,4), (2,4), (3,4)]

def nine():
	return [(0,0), (1,0), (2,0), (3,0), (0,1), (3,1), (0,2), (1,2), (2,2), (3,2), (3,3), (3,4)]


def tick():
	return [(7,0), (6,1), (7,1),(5,2), (6,2), (7,2), (0,3), (1,3), (4,3), (5,3), (6,3), (7,3), (0,4), (1,4), (2,4), (3,4), (4,4), (5,4), (6,4), (0,5), (1,5), (2,5), (3,5), (4,5), (5,5),(1,6), (2,6), (3,6), (4,6), (2,7), (3,7)] 

def cross():
	return[(0,0), (1,0), (6,0), (7,0), (0,1), (1,1), (2,1), (5,1), (6,1), (7,1), (1,2), (2,2), (3,2), (4,2), (5,2), (6,2), (2,3), (3,3), (4,3), (5,3), (2,4), (3,4), (4,4), (5,4), (1,5), (2,5), (3,5), (4,5), (5,5), (6,5), (0,6), (1,6), (2,6), (5,6), (6,6), (7,6), (0,7), (1,7), (6,7), (7,7)]

def warning():
	return[(3,0), (4,0), (3,1), (4,1), (3,2), (4,2), (3,3), (4,3), (3,4), (4,4), (3,6), (4,6), (3,7), (4,7)]

def smileyAngry(color,bgcolor):
	image = Image.new('RGB', (8, 8))
	draw = ImageDraw.Draw(image)

	draw.rectangle(((0,0),(7,7)),outline=color,fill=bgcolor)

	draw.point((2,2),fill=color)
	draw.point((5,2),fill=color)
	
	draw.rectangle((2,4,5,5),fill=color)

	return image

unicornhat.brightness(0.3)
unicornhat.rotation(180)

smileys = []

sad = smileySad((255,255,0), (0,0,0))
sadAlt = smileySad((0,0,0), (255,255,0))

happy = smileyHappy((0,255,0), (0,0,0))
happyAlt = smileyHappy((0,0,0), (0,255,0))

angry = smileyAngry((255,0,0), (0,0,0))
angryAlt = smileyAngry((0,0,0), (255,0,0))

smileySets = [[happy, happyAlt], [sad, sadAlt], [angry, angryAlt]]

# for coord in cross():
# 	unicornhat.set_pixel(coord[0], coord[1], 255, 0, 0)

# unicornhat.show()
# time.sleep(4)
# unicornhat.off()

figures = []
figures.append(zero())
figures.append(one())
figures.append(two())
figures.append(three())
figures.append(four())
figures.append(five())
figures.append(six())
figures.append(seven())
figures.append(eight())
figures.append(nine())
figures.append(tick())
figures.append(cross())
figures.append(warning())


# for figure in figures:
# 	color = (randint(30,255), randint(30,255), randint(30,255))
	
# 	for coord in figure:
# 		unicornhat.set_pixel(coord[0], coord[1], *color)
# 	unicornhat.show()

# 	time.sleep(0.5)

# 	unicornhat.off()

# for rowOffset in range(7, (len(figures) * 5) + 7, -1):
# 	for figure in figures:
# 		figureOffset = 5 * figures.index(figure)
# 		xOffset = rowOffset + figureOffset

# 		if xOffset >= 0 and xOffset <= 7:
# 			# La figura esta todo o en parte dentro

# 			# Limpiamos su 5x4
# 			for xClean in range(xOffset, xOffset + 5):
# 				if xClean >= 0 and xClean <= 7:
# 					for yClean in (0, 5):
# 						unicornhat.set_pixel(xClean, yClean, 0, 0, 0)



# 			for coord in figure:
# 				x = coord[0] + xOffset
# 				if x >= 0 and x <= 7:
# 					unicornhat.set_pixel(x, coord[1], *color)	

# 	unicornhat.show()

# 	time.sleep(0.5)


# for coord in warning():
# 	unicornhat.set_pixel(coord[0], coord[1], 255, 255, 0)

# unicornhat.show()
# time.sleep(4)
# unicornhat.off()

while(True):
	for smileySet in smileySets:
		for x in range (0,10):
			for index in range(0,2):
				set_image(smileySet[index])
				unicornhat.show()
				time.sleep(0.3)
